# 動作確認済みの環境 (Verified Environments) #

* Server
    * Redmine-2.3.x
    * Redmine-2.6.6
    * Redmine-3.0.x
    * Redmine-3.1.0
* Client
    * Chrome 43.0.2357.124 (64-bit) on Mac
    * Chrome 43.0.2357.132 (64-bit) on Windows
