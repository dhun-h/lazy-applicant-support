# Dependence on Redmine #

The operation of the plug-in, the following is a prerequisite.


### URL of the Ticket ###

That the URL of the ticket registration falls under any.  
In this other URL, the plug-in does not function at all.

* [http|https]://\*/\*/projects/\*/issues/new
* [http|https]://\*/projects/\*/issues/new


## HTML ##

### HTMLのid/name/value ###
```js
const ID_REPLACEMENT_ROOT = '#all_attributes';
const ID_ISSUE_FORM = '#issue-form';
const ID_TRACKER = '#issue_tracker_id';
const ID_TITLE = '#issue_subject';
const ID_DESC  = '#issue_description';
const ID_STATUS  = '#issue_status_id';
const ID_ASSIGNER = '#issue_assigned_to_id';
const ID_START_DATE = '#issue_start_date';
const ID_DUE_DATE = '#issue_due_date';
const ID_WATCHERS_FORM = '#watchers_form';
const ID_WATCHERS_INPUTS = '#watchers_inputs';
const ID_PREFIX_WATCHER_LABEL = 'issue_watcher_user_ids_';
const ID_DATE_FIELD_SET = [ID_START_DATE.substr(1), ID_DUE_DATE.substr(1)];

const NM_TITLE = 'issue[subject]';
const NM_DESC  = 'issue[description]';
const NM_STATUS = 'issue[status_id]';
const NM_ASSIGNER = 'issue[assigned_to_id]';
const NM_PRIORITY = 'issue[priority_id]';
const NM_WATCHER_CHECK = 'issue[watcher_user_ids][]';

const DATE_FIELD_CLASS = 'hasDatepicker';
const DATE_PICKER_BUTTON_CLASS = 'ui-datepicker-trigger';
```


### Tracker's　DOM ###
```html
<p>
    <label />
    <select id="issue_tracker_id" />
</p>
```


### Assignee's DOM ###
```html
<p>
    <label />
    <select id="issue_assigned_to_id" />
</p>
```


### Watcher's DOM ###
```html
<p id="watchers_form">
    <label />
    <span id="watchers_inputs" />
    <span class="search_for_watchers" />
</p>
```


### Date field's DOM ###
```html
<p>
    <label />
    <input class="hasDatepicker" />
    <img class="ui-datepicker-trigger" />
</p>
```
