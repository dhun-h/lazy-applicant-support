#!/bin/bash

cd `echo $(cd $(dirname $0);pwd)`

cp 1024x640-post-introduction-issue-basic.png    1280x800-post-introduction-issue-basic.png
cp 1024x640-post-introduction-issue-expand.png   1280x800-post-introduction-issue-expand.png
cp 1024x640-post-introduction-options-basic.png  1280x800-post-introduction-options-basic.png
cp 1024x640-post-introduction-options-expand.png 1280x800-post-introduction-options-expand.png
cp 1024x640-pre-introduction-issue.png           1280x800-pre-introduction-issue.png
