# README

Lazy Applicant.  
With this plugin, Redmine ticket you can easily register.

### How to Use ###

When you view the ticket registration form, plugin to run.
It will be the same procedure as the previous Ticket.


## Feature

In ticket registration form, and provides input assistance such as template features.

### Basic features

* Tracker template  
  The input contents of the entire ticket registration form, you can be registered as a template.　(But **status field just can not be saved**)  
  Templates are managed for each tracker.
* Default tracker template  
  When you change the tracker, you can specify the automatically applied template.


### Extended features

By changing the "Use features" to "All features" in Options, all the features of the plugin are available.

* Used assignee  
  It will list the only assignee who carried out the following:
    * Assignee that was registered to the tracker of template
    * Assignee that was specified when ticket creation
* Watcher template  
  The designated watcher, you can be registered as a template.  
  Templates are managed for each project


### Note

* The registered template and the default can be edited in the Options
* Settings that have been changed in the Options, will take effect from the next ticket registration form.  
  Please re-open the ticket registration form.
* This plug-in provides the only input auxiliary ticket registration of Redmine  
  **It does not interfere with the Redmine ticket registration feature**
* It does not work with the ticket update. (Because I do not think that there is a demand)


## Screen image
### Basic features
![](images/1280x800-post-introduction-issue-basic.png "Ticket registration")

![](images/1280x800-post-introduction-options-basic.png "Options")

### Extended features
![](images/1280x800-post-introduction-issue-expand.png "Ticket registration")

![](images/1280x800-post-introduction-options-expand.png "Options")

### Ticket registration (Reference materials)
![](images/1280x800-pre-introduction-issue.png "Ticket registration \(Reference materials\)")


## Dependence on Redmine
* [here](dependency-to-redmine.md)
