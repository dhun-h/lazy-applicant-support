# Release notes #

### v2.3.0 ###
2016-02-23

* Modified to display the application number of days in the field transfer vacation or an alternative vacation.  
  ⇒  feature for Ars-Nova.co.jp only

### v2.2.0 ###
2015-09-28

* Remove the statistical analysis by GoogleAnalytics.

### v2.1.0 ###
2015-09-19

* UI improved
    * Select the features to use (simple mode / All features)
    * Highlight the item that you added in the plugin to the ticket form
    * Add navigation to support site in the Options
* Remove confusing features
    * Default features of the assignee
    * Default features of the watcher' template
* Bug fixes
    * Resolves a memory leak in the background page

### v2.0.2 ###
2015-09-14

* Bug fixes
    * Fixed behavior of when you manually enter the date field.

### v2.0.1 ###
2015-09-13

* Bug fixes
    * Corrected the URL of the Developer website

### v2.0.0 ###
2015-09-13

* You have to be able to manage the input contents of the entire form as a template
* As default value of the date field, ybe able to set the system date.
* UI improved  
  The default changes or template registration, it was to allow as an explicit user operation
* Explicit registration amount of data
    * Show registration amount of data and the upper limit to the Options page
    * When reaching the registered data amount upper limit of the storage and so as to feed back to the user
* The general published to the Chrome Web Store

### v1.8.0 ###
2015-08-20

* Opt-out support of GoogleAnalytics
* i18n support
* We decided the general Published to the Chrome Web Store  
  In Chrome for Windows, in order to when you restart Nora crx is disabled. I did not notice at all orz

### v1.7.0 ###
2015-08-15

* You have to be able to specify a default in watcher template

### v1.6.0 ###
2015-08-09

* Supports Redmine3.x system  
  Hurriedly support for Ars-Nova of Redmine had been migrated to suddenly 3.x system orz

### v1.5.0 ###
2015-08-05

* Multi-domain support (modified to work even Redmine other than Ars-Nova)
* Change default so that it can be managed for each tracker
* Refactoring the appearance of the options page
* Start a statistical analysis by GoogleAnalytics

### v1.4.0 ###
2015-07-26

* Add feature of watcher's template

### v1.3.0 ###
2015-07-20

* Added an option page

### v1.2.0 ###
2015-07-20

* Add the used assignee

### v1.1.0 ###
2015-07-15

* Even at the time of change of the tracker (such as paid application and equipment purchase), it has to be able to use the initial value set and radio buttons

### v1.0.0 ###
2015-07-12

* Published
