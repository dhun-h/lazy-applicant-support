# README

Lazy Applicant.  
With this plugin, Redmine ticket you can easily register.

You can save the input contents of the ticket registration form as a template.
The saved template, you can call the next time later Ticket.

All processing will have to complete on the client side.
Do not need modify server setting, install server side plugin, Redmine-WebAPI.


## How to Use

### Install

1. Install the plugin to your Chrome.  
⇒ At this time, there is no noticeable changes to your PC

### Save template

1. Access the ticket registration form of Redmine.  
    ⇒ Added part by plugins sway from side to side
1. Ticket registration form, edit the content you want to save as a template.
1. Enter the name of the template you want to save.
1. Click the Save icon.  
    ⇒ template is stored in the plug-in

### Use template

1. Access the ticket registration form of Redmine.
1. By calling a saved template, and then restore the ticket registration form on the state of the template.


## Features

[here](feature-for-redmine/README.md)


## How to install
1. Access to [Chrome Web Store](https://chrome.google.com/webstore/detail/redmine-lazy-applicant/dofmgcleeckgckfapnhookpocglpbmgp?hl=en).
1. Click on the "ADD TO CHROME" button in the upper right-hand corner.  
   ![](images/install-01.png "")
1. A confirmation dialog is displayed, click the "Add extension" button.  
   ![](images/install-02.png "")


## Notes
* Where it is used, please register Make sure all of the entries, including the initial setting.
* All plug risk of any damages that occur by using the in-(monetary damages and physical damage and the other program will not work) to burden the user,  
  software producer shall not bear the responsibility.
  Even if that is prohibited legally limitation of liability, software makers assume that you do not pay the damages that any occurred.


## License
This plug-in, available at the [No license](../license.txt).

## Release notes
[here](misc/release-note.md)

## Verified Environments
[here](../_general/verified-environments.md)

## Contributors
[here](../_general/contributors.md)

## Special thanks
[here](../_general/special-thanks.md)
