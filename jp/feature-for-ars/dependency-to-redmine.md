# Redmineへの依存性 #

アルスノヴァ社の社内申請プロジェクト向け拡張機能では、[一般的なRedmineでの依存性](../feature-for-redmine/dependency-to-redmine.md)
に加え、下記も前提条件となります.


## HTML ##

### HTMLのid/name/value ###
```js
const TRACKER_ID_UQ       = '7';  // 有給休暇
const TRACKER_ID_FURIQ    = '8';  // 振替休暇
const TRACKER_ID_DAIQ     = '9';  // 代替休暇
const TRACKER_ID_BIRTHDAY = '10'; // 誕生日休暇
const TRACKER_ID_BOOKS    = '11'; // 書籍購入
const TRACKER_ID_JUKEN    = '13'; // 資格受験
const TRACKER_ID_SEISAN   = '14'; // 立替精算
const TRACKER_ID_YOYAKU   = '15'; // 会議室依頼
const TRACKER_ID_KOUNYU   = '12'; // 機器購入

const TRACKER_NM_UQ       = '有給休暇';
const TRACKER_NM_FURIQ    = '振替休暇';
const TRACKER_NM_DAIQ     = '代替休暇';
const TRACKER_NM_BIRTHDAY = '誕生日休暇';
const TRACKER_NM_BOOKS    = '書籍購入';
const TRACKER_NM_JUKEN    = '資格受験';
const TRACKER_NM_SEISAN   = '立替精算';
const TRACKER_NM_YOYAKU   = '会議室依頼';
const TRACKER_NM_KOUNYU   = '機器購入';

const ID_APP_DAYS = '#issue_custom_field_values_19';
// Redmineで２つの要素に同じIDを割り当てているバグがあるため、名前でアクセス
// const ID_NEGOTIATED = '#issue_custom_field_values_20';
const NM_NEGOTIATED = 'issue[custom_field_values][20]';

const NM_DELIVERY_ARRANGED = 'issue[custom_field_values][2]';
const NM_DELIVERY_DESTINATION = 'issue[custom_field_values][3]';
const NM_EXAM_PROCEDURE = 'issue[custom_field_values][22]';

const VAL_STATUS_NEW = '1';
const VAL_PRIORITY_NORMAL = '2';

const VAL_DELIVERY_ARRANGED_AFFAIRS = '総務手配';
const VAL_DELIVERY_DESTINATION_HOME = '自宅配送';
const VAL_EXAM_PROCEDURE_OWN = '各自手続き';
const VAL_NEGOTIATED = '1';
```


### 説明のDOM ###
```html
<span id="issue_description_and_toolbar">
    <div class="jstElements" />
    <div class="jstEditor">
        <textarea id="issue_description" />
    </div>
</span>
```


### 申請日数のDOM ###
```html
<p>
    <label />
    <input id="issue_custom_field_values_19" />
</p>
```
