## 画像加工手順 ##
1. ウィンドウサイズ＝1024x640でスクリーンショット
1. プレビューで、罫線を描画
1. プレビューで、サイズ＝1280x800にリサイズ
1. プレビューで、コメント追記. font=24
1. プレビューで、サイズ＝640x400にリサイズ


## 英語のコメント ##
You can save the input contents of the entire form as a template.
If the specified default, it will automatically apply a template when you display the ticket registration screen to form.

You can save the watchers as a template.

The list only the assignee to be utilized.


## 日本語のコメント ##
フォーム全体の入力内容をテンプレートとして保存できます.
規定値にすれば、チケット登録画面を表示したときに自動的にテンプレートをフォームへ反映します.

ウォッチャーをテンプレートとして保存できます.

利用したことがある
担当者だけを一覧表示


## 日本語のコメント(for ArsNova) ##
説明は
ラジオで選択可

申請日数は
ラジオで選択可

チェック済み

## 英語のサンプル

```
BugReport: iPhone
iPhone:
h2. Events
Please write as specific as possible.

h2. Steps to Reproduce
#
#

h2. Reproduction environment
IT or RC or Production
```

## 日本語のサンプル

```
不具合報告：iPhone
【iPhone】
h2. 事象
できるだけ具体的に書いてください

h2. 再現手順
#
#
#

h2. 再現環境
```
